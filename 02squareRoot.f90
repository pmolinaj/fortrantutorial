      PROGRAM sqroot
      ! -------------------------------------------------------------------      
      ! Author: Pedro Molina Jiménez                       Date: 04/11/2014
      ! Purpose:
      !      Given a list of N numbers, find the square root of the
      !      addition of their squares.
      ! Record of revision:
      !      DATE         PROGRAMMER          DESCRIPTION OF CHANGE
      !      ====         ==========          =====================
      !     04/11/14      PMJ                 Original code.
      !      
      ! Notes: 
      ! Example 02 of the algorithm course in the Polytechnical University 
      ! of Madrid. Original Code in Basic provided by Prof. Gavete Corvinos
      ! Engineering School of Mining. UPM. Madrid. Spain.
      ! -------------------------------------------------------------------
       
       ! Declare Variables
       
       implicit none
       
       integer :: n, i, j
       real(kind=8) :: suma,raizCuad
       real(kind=8), dimension(20) :: x

       ! Input data
       print *, "Compute the sqrt of the Addition of the squares of "
       print *, "N numbers."
       print *, "----------------------------------"
       print *, " "
       print *, "Input N"
       read *, n
       do i=1, n
          print *, "Input the number",i  
          read (*,*) X(i)
       end do

       ! Calculations
       suma = 0.0
       do j = 1, n
          X(j) =  X(j) * X(J)
          suma = suma + X(j) 
       end do
       raizCuad = sqrt(suma)

       ! Results
       print *, "Results"
       print *, "-------"
       print *, "The addition of the squares is = ", suma
       print *, " "
       print *, "The square root of the addition of the "
       print *, "squares is: ", raizCuad

     end program sqroot
