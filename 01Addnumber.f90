      PROGRAM ADD
      ! -------------------------------------------------------------------      
      ! Author: Pedro Molina Jiménez                       Date: 04/11/2014
      ! Purpose:
      !      Given a list of n numbers, calculate their addition
      ! Record of revision:
      !      DATE         PROGRAMMER          DESCRIPTION OF CHANGE
      !      ====         ==========          =====================
      !     04/11/14      PMJ                 Original code.
      !      
      ! Notes: 
      ! Example 01 of the algorithm course in the Polytechnical University 
      ! of Madrid. Original Code in Basic provided by Prof. Gavete Corvinos
      ! Engineering School of Mining. UPM. Madrid. Spain.
      ! -------------------------------------------------------------------

      ! Given N numbers compute their addition
      
      !+++++this is a little change 
      
      ! Declare Variables
       
       implicit none
       
       integer :: n
       integer :: i,j
       real :: addition
       real(kind=8) :: x(8)

       ! Input data
       print *, "Compute the Addition of N numbers."
       print *, "----------------------------------"
       print *, " "
       print *, "Input N"
       read *, n
       do i=1,n
          print *, "Input the number",i  
          read (*,*) X(i)
       end do

       ! Calculations
       addition = 0.0
       do j = 1, n
          addition = addition + x(j)
       end do

       ! Results
       print *, "---------------"
       print *, "Addition = ",  addition

     END PROGRAM ADD
